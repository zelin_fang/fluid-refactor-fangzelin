package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.enums.MqType;
import com.paic.arch.jmsbroker.exception.NoMessageReceivedException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 *1、按功能职责对项目代码进行拆分组合，使得每个类的职责单一清晰
 * 2、定义mq发送接口及接收接口，充分考虑其拓展性，便于接入其他mq代理
 * 3、使用工厂类对mq具体实现类进行组合，提高代码可用性
 * 当然，项目的构架需要充分考虑性能，安全，可用性，可读性，可拓展性，
 * 如在性能上是否可以进一步优化，采用线程池，多线程的方式来提高性能等等，因时间有限只能简单进行代码拆分组合
 */
public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static JmsMessageBrokerSupport JMS_SUPPORT;

    @BeforeClass
    public static void setup() throws Exception {
        JmsMessageBrokerSupportExample.runningEmbeddedBrokerOnAvailablePort();
        JMS_SUPPORT =   JmsMessageFactory.createJmsMessageBrokerSupport(JmsMessageBrokerSupportExample.getBrokerUrl(), MqType.ActiveMq);;
    }

    @AfterClass
    public static void teardown() throws Exception {
        JmsMessageBrokerSupportExample.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        JMS_SUPPORT
                .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
        long messageCount = JmsMessageBrokerSupportExample.getJmsMessageService().getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = JMS_SUPPORT
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }

    @Test(expected = NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        JMS_SUPPORT.retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
    }


}