package com.paic.arch.jmsbroker;

/**
 * Created by zelin on 2018/3/7.
 */
public interface JmsMessageReceiver {
    String retrieveASingleMessageFromTheDestination(String aDestinationName);

    String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout);
}
