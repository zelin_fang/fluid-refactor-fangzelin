package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.activemq.ActiveMqReceiver;
import com.paic.arch.jmsbroker.activemq.ActiveMqSender;
import com.paic.arch.jmsbroker.enums.MqType;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * 工厂类
 * Created by zelin on 2018/3/7.
 */
public class JmsMessageFactory {

    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);

    public static JmsMessageBrokerSupport createJmsMessageBrokerSupport(String brokerUrl, MqType type) throws Exception {
        LOG.debug("Creating a new broker at {}", brokerUrl);
        JmsMessageReceiver jmsMessageReceiver= null ;
        JmsMessageSender jmsMessageSender = null;
        switch (type) {
            case ActiveMq:
                jmsMessageReceiver = ActiveMqReceiver.newInstance(brokerUrl);
                jmsMessageSender = ActiveMqSender.newInstance(brokerUrl);
                break;
            default:
                throw new IllegalArgumentException("type:" + type + " is  unsupported ,please check the code and try");
        }
        JmsMessageBrokerSupport broker = JmsMessageBrokerSupport.newInstance(jmsMessageReceiver,jmsMessageSender);
        return broker;
    }

    public static JmsMessageBrokerSupport createJmsMessageBrokerSupport(JmsMessageReceiver jmsMessageReceiver,JmsMessageSender jmsMessageSender) throws Exception {
        if (jmsMessageReceiver == null || jmsMessageSender == null) {
            throw new IllegalArgumentException(" param of  createJmsMessageBrokerSupport  must not be null");
        }
        JmsMessageBrokerSupport broker = JmsMessageBrokerSupport.newInstance(jmsMessageReceiver,jmsMessageSender);
        return broker;
    }
}
