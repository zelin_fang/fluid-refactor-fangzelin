package com.paic.arch.jmsbroker;

import org.apache.activemq.broker.region.DestinationStatistics;

/**
 * Created by zelin on 2018/3/7.
 */
public interface JmsMessageService {

    void start() throws Exception;
    void stop() throws Exception;



    long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;

    DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception;

    boolean isEmptyQueueAt(String aDestinationName) throws Exception;
}
