package com.paic.arch.jmsbroker.activemq;

import com.paic.arch.jmsbroker.JmsMessageSender;
import org.slf4j.Logger;

import javax.jms.MessageProducer;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by zelin on 2018/3/7.
 */
public class ActiveMqSender extends ActiveMqBase  implements JmsMessageSender{
    private static final Logger LOG = getLogger(ActiveMqBase.class);
    private String brokerUrl;

    public ActiveMqSender(String brokerUr){
        this.brokerUrl = brokerUr;
    }

    public static ActiveMqSender newInstance(String brokerUrl){
        ActiveMqSender ret = new ActiveMqSender(brokerUrl);
        return ret;
    }

    @Override
    public String sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) {
        return  executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(aMessageToSend));
            producer.close();
            return "";
        });
    }

}
