package com.paic.arch.jmsbroker.activemq;

import com.paic.arch.jmsbroker.JmsMessageReceiver;
import com.paic.arch.jmsbroker.exception.NoMessageReceivedException;

import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

/**
 * Created by zelin on 2018/3/7.
 */
public class ActiveMqReceiver  extends ActiveMqBase implements JmsMessageReceiver {

    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    private String brokerUrl;
    public ActiveMqReceiver(String brokerUr){
        this.brokerUrl = brokerUr;
    }

    public static ActiveMqReceiver newInstance(String brokerUrl){
        ActiveMqReceiver ret = new ActiveMqReceiver(brokerUrl);
        return ret;
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(aTimeout);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            consumer.close();
            return ((TextMessage) message).getText();
        });
    }
}
