package com.paic.arch.jmsbroker;

/**
 * Created by zelin on 2018/3/7.
 */
public interface JmsMessageSender {
    String sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend);
}
